// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.metadata.method.test;

import 'package:constrain/constraint.dart';
import 'package:constrain/metadata.dart';
import 'package:test/test.dart';
import 'package:option/option.dart';
import 'domain_model.dart';

main() {
  TypeDescriptorResolver t = new TypeDescriptorResolver();

  group('resolveForFunction', () {
    test('when method has no constraints should return None', () {
      expect(t.resolveForFunction(new MrNoFuncConstraints().mrNoConstraints),
          equals(const None()));
    });

    runSingleMethodConstraintTest(MrSingleFuncConstraint instance) {
      descOpt() => t.resolveForFunction(instance.singleConstraint);
      desc() => descOpt().get();

      test('that is some', () {
        expect(descOpt(), new isInstanceOf<Some>());
      });

      test('that is not null', () {
        expect(desc(), isNotNull);
      });

      test('with non empty positional parameters descriptors', () {
        expect(desc().positionalParameterDescriptors, isNot(isEmpty));
      });

      test(
          'with positional parameters descriptors length == function parameter length',
          () {
        expect(desc().positionalParameterDescriptors, hasLength(2));
      });

      test('with a single positional parameter descriptor that is Some', () {
        expect(desc().positionalParameterDescriptors.where((o) => o is Some),
            hasLength(1));
      });

      test('with the first parameter descriptor that is Some', () {
        expect(
            desc().positionalParameterDescriptors[0], new isInstanceOf<Some>());
      });

      test('with empty named parameters descriptors', () {
        expect(desc().namedParameterDescriptors, isEmpty);
      });

      test('with one constraint on first parameter', () {
        expect(
            desc().positionalParameterDescriptors[0]
                .get()
                .constraintDescriptors,
            hasLength(1));
      });

      test('with expected constraint on parameter', () {
        expect(
            desc().positionalParameterDescriptors[0]
                .get()
                .constraintDescriptors
                .first
                .constraint,
            new isInstanceOf<NotNull>());
      });

      test('with no type descriptor on parameter', () {
        expect(desc().positionalParameterDescriptors[0].get().typeDescriptor,
            equals(const None()));
      });

      test('with caching of descriptors', () {
        expect(desc(), same(desc()));
      });
    }

    group(
        'when method has a parameter constraint should return a FunctionDescriptor',
        () {
      runSingleMethodConstraintTest(new MrSingleFuncConstraint());
    });

    group(
        'when method inherited from superclass has a parameter constraint should return a FunctionDescriptor',
        () {
      runSingleMethodConstraintTest(new MrInheritedSingleFuncConstraint());
    });

    group(
        'when method inherited from interface has a parameter constraint should return a FunctionDescriptor',
        () {
      runSingleMethodConstraintTest(
          new MrInheritedSingleFuncOnInterfaceConstraint());
    });

    group(
        'when method inherited from mixin has a parameter constraint should return a FunctionDescriptor',
        () {
      runSingleMethodConstraintTest(
          new MrInheritedSingleFuncOnMixinConstraint());
    });

    runSingleMethodWithAdditionalConstraintTest(
        MrSingleFuncConstraint instance) {
      descOpt() => t.resolveForFunction(instance.singleConstraint);
      desc() => descOpt().get();

      test('that is some', () {
        expect(descOpt(), new isInstanceOf<Some>());
      });

      test('that is not null', () {
        expect(desc(), isNotNull);
      });

      test('with non empty positional parameters descriptors', () {
        expect(desc().positionalParameterDescriptors, isNot(isEmpty));
      });

      test(
          'with positional parameters descriptors length == function parameter length',
          () {
        expect(desc().positionalParameterDescriptors, hasLength(2));
      });

      test('with two positional parameter descriptors that is Some', () {
        expect(desc().positionalParameterDescriptors.where((o) => o is Some),
            hasLength(2));
      });

      test('with the first parameter descriptor that is Some', () {
        expect(
            desc().positionalParameterDescriptors[0], new isInstanceOf<Some>());
      });

      test('with empty named parameters descriptors', () {
        expect(desc().namedParameterDescriptors, isEmpty);
      });

      test('with two constraints on first parameter', () {
        expect(
            desc().positionalParameterDescriptors[0]
                .get()
                .constraintDescriptors,
            hasLength(2));
      });

      test('with expected constraints on first parameter', () {
        expect(
            desc().positionalParameterDescriptors[0]
                .get()
                .constraintDescriptors
                .map((cd) => cd.constraint.runtimeType)
                .toList(),
            unorderedEquals([NotNull, Ensure]));
      });

      test('with expected constraints on second parameter', () {
        expect(
            desc().positionalParameterDescriptors[1]
                .get()
                .constraintDescriptors
                .map((cd) => cd.constraint.runtimeType)
                .toList(),
            unorderedEquals([NotNull]));
      });

      test('with no type descriptor on parameter', () {
        expect(desc().positionalParameterDescriptors[0].get().typeDescriptor,
            equals(const None()));
      });

      test('with caching of descriptors', () {
        expect(desc(), same(desc()));
      });
    }

    group(
        'when overriding method has additional constraints should return a FunctionDescriptor',
        () {
      runSingleMethodWithAdditionalConstraintTest(
          new MrInheritedSingleFuncConstraintWithExtraConstraint());
    });

    group(
        'when overriding method has additional constraints should return a FunctionDescriptor (interface)',
        () {
      runSingleMethodWithAdditionalConstraintTest(
          new MrInheritedSingleFuncOnInterfaceConstraintWithExtraConstraint());
    });

    group(
        'when overriding method has additional constraints should return a FunctionDescriptor (mixin)',
        () {
      runSingleMethodWithAdditionalConstraintTest(
          new MrInheritedSingleFuncOnMixinConstraintWithExtraConstraint());
    });
  });

  group('should ignore private members', () {
    descOpt() => t.resolveFor(MrPrivateFuncConstraints);
    desc() => descOpt().get();
    mm() => desc().methodDescriptorsMap;
    someMemDesc() => mm()[#someConstraints];

    test('', () {
      expect(descOpt(), new isInstanceOf<Some>());
      expect(mm(), hasLength(1));
      expect(someMemDesc(), isNotNull);
    });
  });

//    new Timer(const Duration(minutes: 10), () {});
}

class MrNoFuncConstraints {
  String mrNoConstraints(int blah) => '$blah';
}

class MrSingleFuncConstraint {
  String singleConstraint(@NotNull() int blah, String foo) => '$blah';
}

class MrInheritedSingleFuncConstraint extends MrSingleFuncConstraint {
  String singleConstraint(int blah, String foo) => '$blah';
}

class MrInheritedSingleFuncOnInterfaceConstraint
    implements MrSingleFuncConstraint {
  String singleConstraint(int blah, String foo) => '$blah';
}

class MrInheritedSingleFuncOnMixinConstraint extends Object
    with MrSingleFuncConstraint {
  String singleConstraint(int blah, String foo) => '$blah';
}

class MrInheritedSingleFuncConstraintWithExtraConstraint
    extends MrSingleFuncConstraint {
  String singleConstraint(
          @Ensure(isBetween10and90) int blah, @NotNull() String foo) =>
      '$blah';
}

class MrInheritedSingleFuncOnInterfaceConstraintWithExtraConstraint
    implements MrSingleFuncConstraint {
  String singleConstraint(
          @Ensure(isBetween10and90) int blah, @NotNull() String foo) =>
      '$blah';
}

class MrInheritedSingleFuncOnMixinConstraintWithExtraConstraint extends Object
    with MrSingleFuncConstraint {
  String singleConstraint(
          @Ensure(isBetween10and90) int blah, @NotNull() String foo) =>
      '$blah';
}

class MrPrivateFuncConstraints {
  String _mrNoConstraints(int blah) => '$blah';

  @NotNull()
  String someConstraints(@NotNull() int blah) => '$blah';
}

ignoreMe() {
  // just to silence analyser
  new MrPrivateFuncConstraints()._mrNoConstraints(10);
}