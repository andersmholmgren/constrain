// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.json.test;

import 'package:test/test.dart';
import 'domain_model.dart';
import 'package:constrain/validator.dart';
import 'dart:convert';

final json = new JsonEncoder.withIndent('  ');

main() {
  group('toJson', () {
    final Person person = new Person()
      ..age = -22
      ..addresses = [new Address()..street = "16 st"];

    final validator = new Validator();

    final violations = validator.validate(person);

    _json() => violations.map((v) => v.toJson()).toList();
//    Map first() => _json()[0];
    Map constraint(int i) => _json()[i]['constraint'];

//    print(json.convert(_json()));

    test('produces constraint json with correct constraint type', () {
      expect(_json(), hasLength(4));

      expect(constraint(0), isNotNull);
      expect(constraint(0)['type'], equals('Ensure'));
    });

    test('produces constraint json with correct constraint description', () {
      expect(constraint(0)['description'], isNull);

      expect(constraint(3), isNotNull);
      expect(
          constraint(3)['description'],
          equals(
              'Must be either older than twenty or have at least two adresses'));
    });

    test('produces constraint json with correct constraint group', () {
      expect(constraint(0)['group'], equals('DefaultGroup'));
    });

    // TODO: more tests
  });
}
