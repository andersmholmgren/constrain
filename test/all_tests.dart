// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.
library constrain.all.test;

import 'package:test/test.dart';
import 'metadata_test.dart' as metadata;
import 'function_metadata_test.dart' as function_metadata;
import 'method_metadata_test.dart' as method_metadata;
import 'validator_test.dart' as validator;
import 'validator_functions_test.dart' as validator_functions;
import 'json_test.dart' as json;
import 'constrained_function_proxy_test.dart' as constrained_function_proxy;
import 'core_constraints_test.dart' as core_constraints;

main() {
  group('[metadata]', metadata.main);
  group('[function_metadata]', function_metadata.main);
  group('[method_metadata]', method_metadata.main);
  group('[validator]', validator.main);
  group('[validator_functions]', validator_functions.main);
  group('[constrained_function_proxy]', constrained_function_proxy.main);
  group('[core_constraints]', core_constraints.main);
  group('[json]', json.main);
}
