// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.metadata.resolver.runtime;

import 'dart:async';
import 'dart:mirrors';

import 'package:constrain/metadata.dart';
import 'package:option/option.dart';
import 'package:quiver/check.dart';

import '../constraint.dart';
import 'util.dart';

import 'package:logging/logging.dart';

final Logger _log = new Logger('constraint.metadata.resolver.runtime');

final TypeMirror _iterableType = reflectType(Iterable);
final TypeMirror _mapType = reflectType(Map);
final TypeMirror _futureType = reflectType(Future);

class RuntimeTypeDescriptorResolver implements TypeDescriptorResolver {
  final Map _cache = <Type, Option<TypeDescriptor>>{};

  // TODO: need some other way to key functions when we move to code
  // generation
  final Map _functionCache = <MethodMirror, Option<FunctionDescriptor>>{};

  // TODO: Does Dart have weak references?

  @override
  Option<TypeDescriptor> resolveFor(Type clazz) {
    _log.finer(() => 'resolveFor($clazz)');

    if (clazz == null) {
      return const None();
    }

    return resolveForClass(reflectClass(clazz));
  }

  @override
  Option<FunctionDescriptor> resolveForFunction(Function function) {
    _log.finer(() => 'resolveForFunction($function)');

    if (function == null) {
      return const None();
    }

    return _resolveForFunctionOrMethod(
        (reflect(function) as ClosureMirror).function);
  }

  @override
  Option<TypeDescriptor> resolveForClass(ClassMirror cm) {
    _log.finer(() => 'resolveForClass($cm)');

    if (cm == null || !cm.hasReflectedType) {
      return const None();
    }

    final cachedDescriptor = _cache[cm.reflectedType];

    if (cachedDescriptor != null) {
      return cachedDescriptor;
    }

    // TODO: is there any risk of concurrent modification. i.e. different futures
    // triggering this?

    /*
     * To cope with type loops we put a proxy in to start with. If we hit a
     * type loop then we will end up actually linking up the proxy to a member.
     * So when we have the real type, we first wire the real descriptor into
     * the proxy before we replace it in the cache. The vast majority of
     * members will be wired into a real descriptor rather than a proxy
     */
    final proxy = new _TypeDescriptorProxy(cm.reflectedType);

    // TODO: this breaks down in more complex models
    // (https://bitbucket.org/andersmholmgren/constraint/issue/34)
    _log.finest(() => 'set proxy for ${cm.reflectedType}');
    _cache[cm.reflectedType] = new Some(proxy);

    final newDescriptor = _resolveForClassUncached(cm);
    if (newDescriptor is Some) {
      proxy.realTypeDescriptor = newDescriptor.get();
    }

    _log.finest(() => 'set real descriptor for ${cm.reflectedType}');
    _cache[cm.reflectedType] = newDescriptor;

    return newDescriptor;
  }

  Option<TypeDescriptor> _resolveForClassUncached(ClassMirror cm) {
    if (cm.isTopLevel && cm.owner.simpleName == #dart.core) {
      return const None();
    }

    final Set<MemberDescriptor> memberDescriptors = _resolveMembers(cm);

    Option<TypeDescriptor> superclassDescriptor = _resolveSuperclass(cm);

    Iterable<TypeDescriptor> superinterfaceDescs = _resolveSuperinterfaces(cm);

    final Set<ConstraintDescriptor> classConstraints = _resolveConstraints(cm);

    final Set<FunctionDescriptor> methodDescriptors = _resolveMethods(cm);

    final hasConstraints = memberDescriptors.isNotEmpty ||
        methodDescriptors.isNotEmpty ||
        classConstraints.isNotEmpty ||
        superclassDescriptor is Some ||
        superinterfaceDescs.isNotEmpty;

    return hasConstraints
        ? new Some(new TypeDescriptor(
            cm.reflectedType,
            classConstraints,
            memberDescriptors,
            methodDescriptors,
            superclassDescriptor,
            superinterfaceDescs))
        : const None();
  }

  Option<TypeDescriptor> _resolveSuperclass(ClassMirror cm) {
    return resolveForClass(cm.superclass);
  }

  Iterable<TypeDescriptor> _resolveSuperinterfaces(ClassMirror cm) {
    return cm.superinterfaces
        .map((i) => optionAsIterable(resolveForClass(i)))
        .expand((e) => e);
  }

  Set<MemberDescriptor> _resolveMembers(ClassMirror cm) {
    final memberDescriptors = new Set<MemberDescriptor>();

    cm.declarations.forEach((k, v) {
      if (!v.isPrivate &&
          ((v is VariableMirror && !v.isStatic) ||
              (v is MethodMirror &&
                  !v.isStatic &&
                  (v.isGetter /* || v.isSetter */)))) {
        final TypeMirror rawMemberType = v is VariableMirror
            ? v.type
            : v is MethodMirror ? v.returnType : throw new StateError('oops');

        var memberDescriptor = _resolveMember(v, rawMemberType, k);

        if (memberDescriptor is Some) {
          memberDescriptors.add(memberDescriptor.get());
        }
      }
    });

    return memberDescriptors;
  }

  Set<FunctionDescriptor> _resolveMethods(ClassMirror cm) {
    final methodDescriptors = new Set<FunctionDescriptor>();

    // TODO: constructors etc
    cm.declarations.forEach((k, v) {
      if (v is MethodMirror &&
          !v.isStatic &&
          !v.isGetter &&
          !v.isSetter &&
          !v.isConstructor) {
        var methodDescriptor = _resolveForFunction(v);

        if (methodDescriptor is Some) {
          methodDescriptors.add(methodDescriptor.get());
        }
      }
    });

    return methodDescriptors;
  }

  Option<MemberDescriptor> _resolveMember(
      DeclarationMirror dm, TypeMirror rawMemberType, Symbol name) {
    final Set<ConstraintDescriptor> constraintDescriptors =
        _resolveConstraints(dm);

    final isCollection = _isCollection(rawMemberType);
    final isFuture = _isFuture(rawMemberType);
    final isGeneric = _isGeneric(rawMemberType);

    if ((isFuture || isCollection) && !isGeneric) {
      return const None(); // nothing to go on here
    }

    // TODO: not sure what to do about more than one type arg.
    // Maybe this should be treated same as no generics?
    final memberType = !(isCollection || isFuture)
        ? rawMemberType
        : rawMemberType.typeArguments.first;

    final Option<TypeDescriptor> memberTypeDescriptor =
        _resolveForType(memberType);

    return (constraintDescriptors.isNotEmpty || memberTypeDescriptor is Some)
        ? new Some(new MemberDescriptor(
            name, constraintDescriptors, memberTypeDescriptor,
            isMultivalued: isCollection,
            isGeneric: isGeneric,
            isFuture: isFuture))
        : const None();
  }

  Option<TypeDescriptor> _resolveForType(TypeMirror tm) {
    return tm is ClassMirror ? resolveForClass(tm) : const None();
  }

  Set<ConstraintDescriptor> _resolveConstraints(DeclarationMirror dm) {
    return dm.metadata
        .where((InstanceMirror im) => im.reflectee is Constraint)
        .map((im) => im.reflectee)
        .map((c) => new ConstraintDescriptor(c))
        .toSet();
  }

  bool _isCollection(TypeMirror tm) =>
      tm is ClassMirror && tm.isSubtypeOf(_iterableType) ||
      tm.isSubtypeOf(_mapType);

  bool _isFuture(TypeMirror tm) =>
      tm is ClassMirror && tm.isSubtypeOf(_futureType);

  bool _isGeneric(TypeMirror tm) =>
      tm is ClassMirror && tm.typeArguments.isNotEmpty;

  Option<FunctionDescriptor> _resolveForFunctionOrMethod(MethodMirror mm) {
    if (mm.owner is ClassMirror) {
      return resolveForClass(mm.owner)
          .expand((td) => new Option(td.methodDescriptorsMap[mm.simpleName]));
    }
    return _resolveForFunction(mm);
  }

  Option<FunctionDescriptor> _resolveForFunction(MethodMirror mm) {
    if (mm == null) {
      return const None();
    }

    final cachedDescriptor = _functionCache[mm];

    if (cachedDescriptor != null) {
      return cachedDescriptor;
    }

    final newDescriptor = _resolveForFunctionUncached(mm);

    _functionCache[mm] = newDescriptor;

    return newDescriptor;
  }

  Option<FunctionDescriptor> _resolveForFunctionUncached(MethodMirror mm) {
    if (mm.isTopLevel && mm.owner.simpleName == #dart.core) {
      return const None();
    }

    final positionalMemberDescriptors = _resolveParameters(mm, false);

    final List<Option<MemberDescriptor>> namedMemberDescriptors =
        _resolveParameters(mm, true);

    final returnDescriptor = _resolveReturn(mm);

    final hasConstraints = positionalMemberDescriptors.any((o) => o is Some) ||
            namedMemberDescriptors.any((o) => o is Some) ||
            returnDescriptor is Some
//        || superclassFunctionDescriptor is Some
//        || superinterfaceFunctionDescs.isNotEmpty
        ;

    final namedParameterDescriptorsMap = <Symbol, MemberDescriptor>{};

    namedMemberDescriptors.forEach((mdOpt) {
      if (mdOpt is Some) {
        final md = mdOpt.get();
        namedParameterDescriptorsMap[md.name] = md;
      }
    });

    return hasConstraints
        ? new Some(new FunctionDescriptor(
            mm.simpleName,
            positionalMemberDescriptors,
            namedParameterDescriptorsMap,
            returnDescriptor
//            superclassFunctionDescriptor, superinterfaceFunctionDescs
            ))
        : const None();
  }

  List<Option<MemberDescriptor>> _resolveParameters(
      MethodMirror mm, bool named) {
    return mm.parameters.where((pm) => pm.isNamed == named).map((pm) {
      return _resolveMember(pm, pm.type, pm.simpleName);
    }).toList(growable: false);
  }

  // TODO: modeling this as a MemberDescriptor currently requires it to have
  // a non null name symbol for the root of the validation, but there isn't
  // a natural root for a return. Need to rethink this. Maybe split out new
  // class that is the same as MemberDescriptor without the symbol.
  // Note #xxxx is just a bogus name as it can't be null
  Option<MemberDescriptor> _resolveReturn(MethodMirror mm) =>
      _resolveMember(mm, mm.returnType, #xxxx);
}

class _TypeDescriptorProxy implements TypeDescriptor {
  TypeDescriptor _realTypeDescriptor;

  _TypeDescriptorProxy(this.type);

  TypeDescriptor get realTypeDescriptor {
    // to prevent it being called before it's initialised
    checkState(_realTypeDescriptor != null,
        message: "realTypeDescriptor on proxy for $type called before "
            "it is initialiased. "
            "May be caused by "
            "https://bitbucket.org/andersmholmgren/constrain/issues/34");
    return _realTypeDescriptor;
  }

  set realTypeDescriptor(TypeDescriptor d) {
    _realTypeDescriptor = d;
  }

  @override
  Set<ConstraintDescriptor> get constraintDescriptors =>
      realTypeDescriptor.constraintDescriptors;

  @override
  Iterable<MemberDescriptor> get memberDescriptors =>
      realTypeDescriptor.memberDescriptors;

  @override
  Map<Symbol, MemberDescriptor> get memberDescriptorsMap =>
      realTypeDescriptor.memberDescriptorsMap;

  @override
  final Type type;

  @override
  Map<Symbol, FunctionDescriptor> get methodDescriptorsMap =>
      realTypeDescriptor.methodDescriptorsMap;

  String toString() => 'TypeDescriptorProxy for $type';
}

// TODO: need to:
// - support inheritence of types
// - difference between simple types and classes
//   - i.e. classes have super types, simple types won't have their own type constraints
