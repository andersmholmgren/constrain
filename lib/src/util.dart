// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library util;

import 'package:option/option.dart';
export 'package:option/option.dart';

Iterable optionAsIterable(Option o) => o is Some ? [o.get()] : const [];
