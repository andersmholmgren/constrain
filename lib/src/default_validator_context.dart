// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.validate.context;

import 'package:constrain/metadata.dart';
import 'package:constrain/constraint.dart';
import 'preconditions.dart';
import 'dart:mirrors';
import 'package:option/option.dart';

abstract class ValidationContext {
  final _StaticContext staticContext;
  final InstanceMirror instanceMirror;
  final ValidationContext parent;
  dynamic get object => instanceMirror.reflectee;

  ValidationContext(this.staticContext, this.instanceMirror, [this.parent]) {
    ensure(instanceMirror, isNotNull);
    ensure(staticContext, isNotNull);
  }

  void validate();

  Iterable<Symbol> get path;

  ValidationContext get root => parent == null ? this : parent.root;
}

class TypeValidationContext extends ValidationContext {
  TypeValidationContext(
      _StaticContext staticContext, InstanceMirror instanceMirror,
      [ValidationContext parent])
      : super(staticContext, instanceMirror, parent);

  TypeValidationContext.root(Object object, TypeDescriptorResolver resolver,
      {ConstraintGroup group: const DefaultGroup()})
      : this(
            new _StaticContext(
                resolver, group == null ? const DefaultGroup() : group),
            reflect(object));

  void validate() {
    if (object != null &&
        !staticContext.haveValidated(instanceMirror.reflectee)) {
      staticContext.validated(instanceMirror.reflectee);
      final typeDescriptorOpt =
          staticContext.resolveForClass(instanceMirror.type);
      typeDescriptorOpt.map((typeDescriptor) {
        _validateMembers(typeDescriptor);
        _validateClassConstraints(typeDescriptor.constraintDescriptors);
      });
    }
  }

  void _validateMembers(TypeDescriptor typeDescriptor) {
    typeDescriptor.memberDescriptors.forEach(_validateMember);
  }

  void _validateClassConstraints(Set<ConstraintDescriptor> constraints) {
    constraints.forEach(_validateConstraint);
  }

  void _validateConstraint(ConstraintDescriptor cd) {
    final constraintContext = new TypeConstraintValidationContext(
        cd, instanceMirror, staticContext, this);

    constraintContext.validate();
  }

  void _validateMember(MemberDescriptor member) {
    pushMember(member).validate();
  }

  MemberValidationContext pushConstraintDesc(MemberDescriptor member) {
    return new MemberValidationContext(
        staticContext, instanceMirror.getField(member.name), member, this);
  }

  MemberValidationContext pushMember(MemberDescriptor member) {
    return new MemberValidationContext(
        staticContext, instanceMirror.getField(member.name), member, this);
  }

  Iterable<Symbol> get path => parent == null ? [] : parent.path;

  String get pathString => _toPathString(path);

  Set<ConstraintViolation> build() => staticContext.violations;
}

class MemberValidationContext extends ValidationContext {
  final MemberDescriptor member;

  MemberValidationContext(_StaticContext staticContext,
      InstanceMirror instanceMirror, this.member, ValidationContext parent)
      : super(staticContext, instanceMirror, parent) {
    ensure(parent, isNotNull);
    ensure(member, isNotNull);
  }

  @override
  void validate() {
    _validateMemberConstraints();
    _validateMemberInstances();
  }

  void _validateMemberConstraints() {
    member.constraintDescriptors.forEach(_validateConstraint);
  }

  void _validateMemberInstances() {
    if (instanceMirror.reflectee != null) {
      pushTypeContexts().forEach((i) => i.validate());
    }
  }

  void _validateConstraint(ConstraintDescriptor cd) {
    final constraintContext = new MemberConstraintValidationContext(
        staticContext,
        cd,
        instanceMirror,
        member,
        parent); // TODO: this or parent?

    constraintContext.validate();
  }

  Iterable<TypeValidationContext> pushTypeContexts() {
    if (!member.isMultivalued) {
      return [new TypeValidationContext(staticContext, instanceMirror, this)];
    }

    if (!(instanceMirror.reflectee is Iterable)) {
      throw new StateError("TODO: Only Iterables currently supported");
    }

    final Iterable i = instanceMirror.reflectee;
    return i
        .map((e) => new TypeValidationContext(staticContext, reflect(e), this));
  }

  Iterable<Symbol> get path =>
      parent == null ? [] : new List.from(parent.path)..add(member.name);

  String get pathString => _toPathString(path);
}

String _toPathString(Iterable<Symbol> path) =>
    path.map((s) => MirrorSystem.getName(s)).join('.');

class MemberConstraintValidationContext extends MemberValidationContext
    implements ConstraintValidationContext {
  final ConstraintDescriptor constraintDescriptor;
  @override
  Constraint get constraint => constraintDescriptor.constraint;

  @override
  get owner => parent.instanceMirror.reflectee;

  MemberConstraintValidationContext(
      _StaticContext staticContext,
      this.constraintDescriptor,
      InstanceMirror instanceMirror,
      MemberDescriptor member,
      ValidationContext parent)
      : super(staticContext, instanceMirror, member, parent) {
    ensure(constraintDescriptor, isNotNull);
  }

  @override
  void validate() {
    if (staticContext.impliesGroup(constraint.group)) {
      constraint.validate(instanceMirror.reflectee, this);
    }
  }

  void addViolation({String reason, ViolationDetails details}) {
    staticContext.addViolation(new ConstraintViolation(constraintDescriptor,
        object, parent.object, path, pathString, root.object,
        reason: reason, details: new Option(details)));
  }
}

// TODO: mixin as code same as MemberConstraintValidationContext
class TypeConstraintValidationContext extends TypeValidationContext
    implements ConstraintValidationContext {
  final ConstraintDescriptor constraintDescriptor;
  @override
  Constraint get constraint => constraintDescriptor.constraint;

  @override
  get owner => instanceMirror.reflectee;

  TypeConstraintValidationContext(
      this.constraintDescriptor,
      InstanceMirror instanceMirror,
      _StaticContext staticContext,
      TypeValidationContext parent)
      : super(staticContext, instanceMirror, parent) {
    ensure(constraintDescriptor, isNotNull);
    ensure(parent, isNotNull);
  }

  @override
  void validate() {
    // TODO: same as in MemberConstraintValidationContext.validate
    if (staticContext.impliesGroup(constraint.group)) {
      constraint.validate(instanceMirror.reflectee, this);
    }
  }

  void addViolation({String reason, ViolationDetails details}) {
    staticContext.addViolation(new ConstraintViolation(constraintDescriptor,
        object, parent.object, path, pathString, root.object,
        reason: reason, details: new Option(details)));
  }
}

class _StaticContext {
  final TypeDescriptorResolver _resolver;
  final Set violations = new Set<ConstraintViolation>();
  final Set _validatedObjects = new Set.identity();
  final ConstraintGroup group;

  _StaticContext(this._resolver, this.group) {
    ensure(_resolver, isNotNull);
  }

//  Option<TypeDescriptor> resolveFor(Type clazz) => _resolver.resolveFor(clazz);

  Option<TypeDescriptor> resolveForClass(ClassMirror cm) =>
      _resolver.resolveForClass(cm);

  Option<FunctionDescriptor> resolveForFunction(Function function) =>
      _resolver.resolveForFunction(function);

  bool haveValidated(Object object) => _validatedObjects.contains(object);

  void validated(Object object) {
    _validatedObjects.add(object);
  }

  void addViolation(ConstraintViolation violation) {
    violations.add(violation);
  }

  bool impliesGroup(ConstraintGroup other) => group.implies(other);
}

class FunctionParameterValidationContext extends ValidationContext {
  final List positionalParameters;
  final Map<Symbol, dynamic> namedParameters;

  FunctionParameterValidationContext(Function object, this.positionalParameters,
      this.namedParameters, TypeDescriptorResolver resolver,
      {ConstraintGroup group: const DefaultGroup()})
      : super(
            new _StaticContext(
                resolver, group == null ? const DefaultGroup() : group),
            reflect(object));

  void validate() {
//    if (object != null &&
//        !staticContext.haveValidated(instanceMirror.reflectee)) {
//      staticContext.validated(instanceMirror.reflectee);
    final functionDescriptorOpt = staticContext.resolveForFunction(object);

    functionDescriptorOpt.map((functionDescriptor) {
      _validatePositionalMembers(
          functionDescriptor.positionalParameterDescriptors);
      _validateNamedMembers(functionDescriptor.namedParameterDescriptorsMap);
    });
  }

  void _validatePositionalMembers(
      List<Option<MemberDescriptor>> positionalParameterDescriptors) {
    new List.generate(positionalParameterDescriptors.length, (i) => i)
        .forEach((i) {
      final value = positionalParameters[i];
      final descriptorOpt = positionalParameterDescriptors[i];
      if (descriptorOpt is Some) {
        _validateMember(descriptorOpt.get(), reflect(value));
      }
    });
  }

  void _validateNamedMembers(
      Map<Symbol, MemberDescriptor> namedParameterDescriptorsMap) {
    namedParameterDescriptorsMap.forEach((k, v) {
      _validateMember(v, reflect(namedParameters[k]));
    });
  }

  void _validateMember(MemberDescriptor member, InstanceMirror im) {
    pushMember(member, im).validate();
  }

  MemberValidationContext pushMember(
      MemberDescriptor member, InstanceMirror im) {
    return new MemberValidationContext(staticContext, im, member, this);
  }

  Iterable<Symbol> get path => parent == null ? [] : parent.path;

  String get pathString => _toPathString(path);

  Set<ConstraintViolation> build() => staticContext.violations;
}

class FunctionReturnValidationContext extends ValidationContext {
  final dynamic returnValue;

  FunctionReturnValidationContext(
      Function object, this.returnValue, TypeDescriptorResolver resolver,
      {ConstraintGroup group: const DefaultGroup()})
      : super(
            new _StaticContext(
                resolver, group == null ? const DefaultGroup() : group),
            reflect(object));

  void validate() {
//    if (returnValue != null &&
//        !staticContext.haveValidated(instanceMirror.reflectee)) {
//      staticContext.validated(instanceMirror.reflectee);

    final functionDescriptorOpt = staticContext.resolveForFunction(object);

    functionDescriptorOpt
        .expand((functionDescriptor) => functionDescriptor.returnDescriptor)
        .map(_validateReturn);

    if (returnValue != null && functionDescriptorOpt is None) {
      // the return type of the method may be dynamic so try validating the
      // return value directly
      final typeValidationContext =
          new TypeValidationContext(staticContext, reflect(returnValue), this);
      typeValidationContext.validate();
    }
  }

  void _validateReturn(MemberDescriptor returnDescriptor) {
    pushMember(returnDescriptor, reflect(returnValue)).validate();
  }

  MemberValidationContext pushMember(
      MemberDescriptor member, InstanceMirror im) {
    return new MemberValidationContext(staticContext, im, member, this);
  }

  Iterable<Symbol> get path => parent == null ? [] : parent.path;

  Set<ConstraintViolation> build() => staticContext.violations;
}
