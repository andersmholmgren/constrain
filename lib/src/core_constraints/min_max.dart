// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.min_max;

import 'common.dart';
import 'package:constrain/src/constraint_group.dart';

/**
 * The annotated element must be a [Comparable] whose value is greater than
 * the specified [min]. If [isInclusive] is true then the value may be also be
 * equal to [min].
 *
 * Supported types are anything [Comparable] such as:
 *
 * * [num]
 * * [DateTime]
 *
 * _Note: doubles incur rounding errors and are therefore not reliably handled._
 *
 *  null elements are considered valid.
 */
class Min extends CoreConstraint<Comparable> {
  /// the minimum value that the value is compared to
  final Comparable min;

  /// if true then the value may also be equal to the [min].
  final bool isInclusive;

  const Min(this.min,
      {this.isInclusive: true,
      ConstraintGroup group: const DefaultGroup(),
      String description})
      : super(group: group, description: description);

  @override
  String get description =>
      super.description ??
      "must be greater than ${isInclusive ? 'or equal to ' : ''}$min";

  @override
  bool isValid(Comparable value) {
    final result = value.compareTo(min);
    return isInclusive ? result >= 0 : result > 0;
  }
}

/**
 * The annotated element must be a [Comparable] whose value is less than
 * the specified [max]. If [isInclusive] is true then the value may be also be
 * equal to [max]
 *
 * Supported types are anything [Comparable] such as:
 *
 * * [num]
 * * [DateTime]
 *
 * _Note: doubles incur rounding errors and are therefore not reliably handled._
 *
 *  null elements are considered valid.
 */
class Max extends CoreConstraint<Comparable> {
  /// the maximum value that the value is compared to
  final Comparable max;

  /// if true then the value may also be equal to the [max].
  final bool isInclusive;

  const Max(this.max, {this.isInclusive: true});

  @override
  String get description =>
      "must be less than ${isInclusive ? 'or equal to ' : ''}$max";

  @override
  bool isValid(Comparable value) {
    final result = value.compareTo(max);
    return isInclusive ? result <= 0 : result < 0;
  }
}

// TODO: why is this not allowed for a const?
//class Range extends CompositeConstraint<num> {
//  const Range(num min, num max,
//              { bool isMinInclusive: true, bool isMaxInclusive: true })
//    : super(const [const Min(min, isInclusive: isMinInclusive),
//                   const Max(max, isInclusive: isMaxInclusive)]);
//}
