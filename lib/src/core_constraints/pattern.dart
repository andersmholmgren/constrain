// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.pattern;

import 'common.dart';

import 'dart:core' as core show Pattern;
import 'dart:core' hide Pattern;
import 'package:constrain/src/constraint_group.dart';

/**
 * The annotated element must be a [String] whose value completely matches
 * the specified [pattern].
 *
 * Supported types are:
 *
 * * [String]
 *
 *  null elements are considered valid.
 */
class Pattern extends CoreConstraint<String> {
  /// The pattern to match against
  final core.Pattern pattern;

  /// If true then the provided [pattern] will be converted into a [RegExp].
  /// This is to make it simpler to use RegExp which is the most common case
  final bool isRegExp;

  const Pattern(core.Pattern pattern,
      {this.isRegExp: true,
      ConstraintGroup group: const DefaultGroup(),
      String description})
      : this.pattern = pattern,
        super(group: group, description: description);

  @override
  String get description => super.description ?? 'must match "$pattern"';

  @override
  bool isValid(String value) {
    final _pattern =
        isRegExp && pattern is! RegExp ? new RegExp(pattern) : pattern;

    // the pattern must match the entire string
    final matches = _pattern.allMatches(value);
    if (matches.length != 1) {
      return false;
    }

    final match = matches.first;
    if (match.start != 0) {
      return false;
    }

    return match.end == value.length;
  }
}
