// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.validate;

import 'constraint.dart';
import 'src/default_validator.dart';
import 'package:constrain/metadata.dart';

/**
 * A validator of objects that contain [Constraint]s. Validations include:
 *
 * * all property constraints, including inherited properties and constraints
 *
 * * all class constraints, including inherited constraints
 *
 * * cascading validation of all non properties
 */
abstract class Validator {
  /**
   * Validates the given [object] returning the set of [ConstraintViolation]s
   * for all the [Constraint]s that were violated, including violations from
   * cascaded validations.
   *
   * If no constraints were violated then an empty set is returned and the
   * object is considered valid.
   *
   * [groups] can be provided to target specific groups of [Constraint]s. If
   * absent the [DefaultGroup] is used
   */
  Set<ConstraintViolation> validate(dynamic object,
      {Iterable<ConstraintGroup> groups});

  Set<ConstraintViolation> validateFunctionParameters(
      Function function, List positionalParameters,
      {Map<Symbol, dynamic> namedParameters, Iterable<ConstraintGroup> groups});

  Set<ConstraintViolation> validateFunctionReturn(
      Function function, returnValue,
      {Iterable<ConstraintGroup> groups});

  factory Validator() => new DefaultValidator(new TypeDescriptorResolver());

/*
 * TODO: This is the methods on Java Beans validator
    <T> Set<ConstraintViolation<T>>  validate(T object, Class<?>... groups)

    <T> Set<ConstraintViolation<T>> validateProperty(T object, String propertyName, Class<?>... groups)

    <T> Set<ConstraintViolation<T>> validateValue(Class<T> beanType, String propertyName, Object value, Class<?>... groups)
 */
}
