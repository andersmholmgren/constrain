// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint;

import 'package:matcher/matcher.dart';
import 'src/preconditions.dart';

import 'src/constraint_violation.dart';
export 'src/constraint_violation.dart';

import 'src/constraint_group.dart';
export 'src/constraint_group.dart';

/// A function based constraint validator that allows control over
/// [ConstraintViolation] creation via the [context].
///
/// The owner of [value] is available via the [context]
typedef void ConstraintValidator(
    dynamic value, ConstraintValidationContext context);

/// A constraint validator that is a simple boolean expression of the [value]
///
/// A return value of true means the constraint is valid
typedef bool SimpleConstraintValidator(dynamic value);

/// A constraint validator that is a simple boolean expression of the [value]
/// and the [owner] of the field (i.e. the object that contains the field being
/// validated).
///
/// A return value of true means the constraint is valid
typedef bool SimplePropertyConstraintValidator(dynamic value, dynamic owner);

/// A function that returns a Matcher to be used for constraint validation
typedef Matcher MatcherCreator();

/**
 * Represents some constraint placed on objects. Constraints may be placed on:
 *
 * * A class. These apply to all instances of the class and are typically used
 * for validation of more than one property (cross field constraints)
 *
 * * A property. These apply to values of that property.
 *
 * * A property getter. Similar to constraints on properties.
 *
 * Constraints may be provided with a [description] which gives control over
 * how the constraint is described. This can make them read better than
 * automatically generated descriptions.
 *
 * Constraints may be placed in a [group] which controls when they will be
 * validated. By default constraints are in the [DefaultGroup].
 *
 * During validation, a set of groups can be supplied which will restrict
 * validation to only constraints implied by that set of groups. The
 * [DefaultGroup] will always be validated.
 */
abstract class Constraint<T> {
  final String description;
  final ConstraintGroup _group;
  ConstraintGroup get group => _group ?? const DefaultGroup();

  const Constraint(
      {this.description, ConstraintGroup group: const DefaultGroup()})
      : this._group = group;

  void validate(T value, ConstraintValidationContext context);

  Map toJson() => {
        'type': runtimeType.toString(),
        'description': description,
        'group': group.runtimeType.toString()
      };
}

/**
 * Indicates that a property is mandatory
 */
class NotNull extends Constraint<dynamic> {
  const NotNull({ConstraintGroup group})
      : super(description: 'Field is mandatory', group: group);

  @override
  void validate(value, ConstraintValidationContext context) {
    if (value == null) {
      context.addViolation();
    }
  }
}

/**
 * Ensure is the main constraint subclass that the vast majority of constraints
 * are likely to use.
 *
 * Ensure delegates the actual validation to it's [validator] object.
 * Note that the [validator] will only be called if the value passed to
 * Ensure's [validate] method is not null.
 *
 * The [validator] can be any of the following types:
 *
 * * [SimpleConstraintValidator]
 * * [SimplePropertyConstraintValidator]
 * * [Matcher]
 * * [MatcherCreator]
 */
class Ensure extends Constraint<dynamic> {
  final dynamic validator;
  const Ensure(this.validator, {String description, ConstraintGroup group})
      : super(description: description, group: group);

  void validate(dynamic value, ConstraintValidationContext context) {
    // TODO: ugly but allows @Ensure(isNotNull) to be used instead of @NotNull()
    // if that is useful
    if (validator == isNotNull) {
      (const NotNull()).validate(value, context);
    } else if (value != null) {
      if (validator is SimpleConstraintValidator) {
        SimpleConstraintValidator v = validator;

        bool isValid = v(value);
        if (!isValid) {
          context.addViolation();
        }
      } else if (validator is SimplePropertyConstraintValidator) {
        SimplePropertyConstraintValidator v = validator;

        bool isValid = v(value, context.owner);
        if (!isValid) {
          context.addViolation();
        }
      } else if (validator is ConstraintValidator) {
        ConstraintValidator v = validator;
        v(value, context);
      } else {
        final _matcher = validator is MatcherCreator ? validator() : validator;

        if (!(_matcher is Matcher)) {
          throw new ArgumentError('validator must be either a Matcher;'
              ' a function that returns a Matcher;'
              ' a ConstraintValidator;'
              ' a SimpleFieldConstraintValidator;'
              ' or a SimpleConstraintValidator');
        }
        var matcher = wrapMatcher(_matcher);
        var matchState = {};
        var failureMessage = null;
        try {
          if (matcher.matches(value, matchState)) return;
        } catch (e, trace) {
          if (failureMessage == null) {
            failureMessage = '${(e is String) ? e : e.toString()} at $trace';
          }
        }
        context.addViolation(
            reason: _createDetails(
                    value, matcher, failureMessage, matchState, false)
                .toString());
      }
    }
  }

  String toString() => validator.toString();
}

/**
 * A context passed to a [Constraint]'s validate method.
 *
 * It provides access to the [constraint] being validated and the object that
 * is the [owner] of the value being validated. In the case of [Constraint]s
 * applied to a class the [owner] is the same as the value.
 *
 * If a constraint is violated the [addViolation] method is used to create
 * the corresponding [ConstraintViolation] object
 */
abstract class ConstraintValidationContext {
  Constraint get constraint;
  dynamic get owner;

  void addViolation({String reason, ViolationDetails details});
}

ViolationDetails _createDetails(
    actualValue, Matcher matcher, String reason, Map matchState, bool verbose) {
  final expected =
      (new StringDescription()..addDescriptionOf(matcher)).toString();
  final actual =
      (new StringDescription()..addDescriptionOf(actualValue)).toString();

  final mismatch = new StringDescription();
  matcher.describeMismatch(actual, mismatch, matchState, verbose);

  final mismatchDescription = mismatch.toString();

//  if (reason != null) {
//    description.add(reason).add('\n');
//  }

  return new ViolationDetails(expected, actual, mismatchDescription);
}
